from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import views
from rest_framework import status
from rest_framework import viewsets, authentication
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User

from models.rooms import Room, RoomAttributes
from serializers import RoomSerializer, \
    UserSerializer, RoomAttributesSerializer
from models.users import Profile
from serializers import ProfileSerializer, ReservationSerializer, PostReservationSerializer
from models.reservations import *
from models.exceptions import *


class ProfileViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer


class UsersViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class RoomsViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer


class RoomAttributesViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = RoomAttributes.objects.all()
    serializer_class = RoomAttributesSerializer


def basic_view(request):
    return render(request, 'base.html', {})


def search_view(request):
    return render(request, 'search.html', {})


class ReservationsView(views.APIView):
    allowed_methods = ['GET', 'POST']
    authentication_classes = (authentication.BasicAuthentication,)

    def get(self, request, pk=None, format=None):
        # start_date = self.request.query_params.get('start_date', None)
        # end_date = self.request.query_params.get('end_date', None)
        # reservations = Reservation.objects.get_all(start_date=start_date,
        #                                            end_date=end_date)
        reservations = Reservation.objects.all()

        response = ReservationSerializer(reservations, many=True).data
        return Response(response)

    def post(self, request, pk=None, format=None):
        serializer = PostReservationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            reservations = Reservation.objects.make_reservations(
                serializer.validated_data
            )
        except NoRoomsWithEnoughCapacityError:
            return Response(status=409, data={
                'status': 1
            })
        except NoRoomsWithMandatoryAttributesError:
            return Response(status=409, data={
                'status': 2
            })
        response = ReservationSerializer(reservations, many=True).data
        return Response(response)

    def delete(self, request, pk=None, format=None):
        instance = get_object_or_404(Reservation, pk=pk)
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class SeriesView(views.APIView):
    authentication_classes = (authentication.BasicAuthentication,)

    def delete(self, request, pk=None, format=None):
        instance = get_object_or_404(ReservationsSeries, pk=pk)
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get(self, request, pk=None, format=None):
        instance = get_object_or_404(ReservationsSeries, pk=pk)
        return Response(status=status.HTTP_204_NO_CONTENT)
