__all__ = ['cached_method']

from functools import wraps
from random import randint

from django.core.cache import cache



def cached_method(func, cache_key=None, in_first_arg=False):
    cache_key = "__cached_".format(
        str(cache_key or randint(0, 100000))
    )

    @wraps(func)
    def wrapper(*args, **kwargs):
        force = kwargs.get('force', False)

        data = not force and (
            getattr(args[0], cache_key, None)
            if in_first_arg
            else cache.get(cache_key)
        )
        if not data:
            data = func(*args, **kwargs)
            if in_first_arg:
                setattr(args[0], cache_key, data)
            else:
                cache.set(cache_key, data)
        return data
    return wrapper