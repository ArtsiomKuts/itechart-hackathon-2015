__all__ = ['Profile']


from django.db import models
from django.conf import settings
from django.utils import timezone

from .rooms import Room
from .constants import ReservationStates


class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    location = models.ForeignKey(Room, null=True, blank=True)

    def __unicode__(self):
        return self.user.username

    def get_location(self, current=False):
        if not current:
            return {
                "location_id": self.location_id,
                "name": self.location.name
            }
        from .reservations import Reservation
        now = timezone.now()
        reservation = Reservation.objects.filter(
            attendees__pk=self.pk,
            start_time__gte=now,
            end_time__lte=now,
            state=ReservationStates.RESERVED.value
        ).first()
        if reservation is None:
            return {
                "location_id": self.location_id,
                "name": self.location.name
            }
        return {
                "location_id": reservation.location_id,
                "name": reservation.location.name
            }

    def get_default_location(self):
        return self.get_location(current=False)

    def get_current_location(self):
        return self.get_location(current=True)
