class BookingException(Exception):
    pass


class NoRoomsWithMandatoryAttributesError(BookingException):
    pass


class NoRoomsWithEnoughCapacityError(BookingException):
    pass
