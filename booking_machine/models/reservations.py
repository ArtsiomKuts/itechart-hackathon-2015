__all__ = ['ReservationsSeries', 'Reservation']

from datetime import timedelta, datetime
from collections import defaultdict
from math import ceil

from django.db import models

from .rooms import Room
from .users import Profile
from .constants import ReservationStates
from .exceptions import *
from booking_machine.utils import BaseManager, dates_compare, ensure_timezone


class ReservationsSeries(models.Model):
    pass


class BaseRoomFilter(object):
    def __init__(self):
        pass

    def process(self, rooms):
        filtered_rooms = (
            room
            for room in rooms
            if self.filter(room)
        )
        return sorted(
            filtered_rooms,
            cmp=self.prefer,
            reverse=True
        )

    def filter(self, room):
        return True

    def prefer(self, room_a, room_b):
        return room_a


class RoomAttrsFilter(BaseRoomFilter):
    def __init__(self, mandatory_attrs, optional_attrs=None):
        super(RoomAttrsFilter, self).__init__()
        self.mandatory_attrs = mandatory_attrs
        self.optional_attrs = optional_attrs

    def filter(self, room):
        return room.has_attrs(self.mandatory_attrs)

    def prefer(self, room_a, room_b):
        if not self.optional_attrs:
            return 0
        a_b_diff = (
            len(room_a.get_missed_attrs(self.optional_attrs)) -
            len(room_b.get_missed_attrs(self.optional_attrs))
        )
        if a_b_diff > 0:
            return -1
        else:
            return 1


class RoomCapacityFilter(BaseRoomFilter):
    def __init__(self, attendees):
        super(RoomCapacityFilter, self).__init__()
        self.required_capacity = len(attendees)

    def filter(self, room):
        return room.capacity >= self.required_capacity

    def prefer(self, room_a, room_b):
        capacity_diff = room_a.capacity - room_b.capacity
        if capacity_diff == 0:
            return 0
        if capacity_diff < 0:
            return 1
        return -1


class ReservationManager(BaseManager):
    def get_by_date(self, start_date, end_date, **kwargs):
        return self.filter(
            start_time__gte=start_date,
            end_time__lte=end_date
        )

    def make_reservations(self, reservation_config):
        rooms = Room.objects.all().prefetch_related('reservations')
        attendees = Profile.objects.filter(
            user__pk__in=reservation_config['attendees']
        ).select_related('user')
        start_date = reservation_config['start_date']
        end_date = reservation_config['end_date']
        start_time = reservation_config['start_time']
        end_time = reservation_config['end_time']
        room_attrs = reservation_config['room_attrs']
        interval = reservation_config['interval']
        subject = reservation_config['subject']
        description = reservation_config['description']
        rooms = RoomAttrsFilter(
            mandatory_attrs=[
                attr['id']
                for attr in room_attrs
                if attr.get('mandatory', False)
                ],
            optional_attrs=[
                attr['id']
                for attr in room_attrs
                if not attr.get('mandatory', False)
                ]
        ).process(rooms)
        if not rooms:
            raise NoRoomsWithMandatoryAttributesError()
        rooms = RoomCapacityFilter(
            attendees=attendees
        ).process(rooms=rooms)
        if not rooms:
            raise NoRoomsWithEnoughCapacityError()

        return self.generate_reservations(
            rooms=rooms,
            start_date=start_date,
            end_date=end_date,
            start_time=start_time,
            end_time=end_time,
            subject=subject,
            interval=interval,
            description=description,
            attendees=attendees
        )

    @staticmethod
    def get_days_iter(start_date, end_date):
        start = start_date
        while dates_compare(start, end_date) < 1:
            yield start
            start += timedelta(days=1)

    def get_room_free_periods(self, rooms, start_date, end_date, interval=None):
        start_date = ensure_timezone(start_date)
        end_date = ensure_timezone(end_date)
        reservations = Reservation.objects.filter(
            room__pk__in=[room.id for room in rooms],
            start_time__gte=start_date,
            end_time__lte=end_date,
            state=ReservationStates.RESERVED.value
        ).order_by('start_time')

        room_time_breakpoints = defaultdict(lambda: [
            {'date': start_date, 'type': 'start'},
            {'date': end_date, 'type': 'end'}
        ])
        for reservation in reservations:
            room_time_breakpoints[reservation.room_id].insert(-1, {
                'date': reservation.start_time,
                'type': 'end'
            })
            room_time_breakpoints[reservation.room_id].insert(-1, {
                'date': reservation.end_time,
                'type': 'start'
            })
        cleaned_room_time_breakpoints = {}
        for room in rooms:
            breakpoints = room_time_breakpoints[room.id]
            cleaned_room_time_breakpoints[room.id] = new_breakpoints = []
            prev_breakpoint = None
            for index, breakpoint in enumerate(breakpoints):
                if prev_breakpoint is not None:
                    dt1 = current_date = ensure_timezone(breakpoint['date'])
                    dt2 = prev_date = ensure_timezone(prev_breakpoint['date'])
                    days = (
                        current_date -
                        prev_date
                    ).days
                    if (dt1.year != dt2.year or dt1.month != dt2.month or
                            dt1.day != dt2.day) and (
                                dt1.minute != dt2.minute or
                                dt1.hour != dt2.hour):
                        days += 1
                    for i in xrange(days):
                        new_breakpoints.append({
                            'date': datetime.combine(
                                (prev_date + timedelta(
                                    days=i)).date(),
                                end_date.time()
                            ),
                            'type': 'end'
                        })
                        new_breakpoints.append({
                            'date': datetime.combine(
                                (prev_date + timedelta(
                                    days=i + 1)).date(),
                                start_date.time()
                            ),
                            'type': 'start'
                        })

                prev_breakpoint = breakpoint
                new_breakpoints.append(breakpoint)
        room_free_periods = defaultdict(list)
        for room_id, breakpoints in cleaned_room_time_breakpoints.iteritems():
            start = None
            for breakpoint in breakpoints:
                if breakpoint['type'] == 'start':
                    start = ensure_timezone(breakpoint['date'])
                elif start and breakpoint['type'] == 'end':
                    end = ensure_timezone(breakpoint['date'])
                    interval_mins = ceil(
                        (
                            ensure_timezone(end) - ensure_timezone(start)
                        ).total_seconds() / 60.0)
                    if interval is None or interval <= interval_mins:
                        room_free_periods[room_id].append({
                            'interval': interval_mins,
                            'start_date': start
                        })
                    end, start = None, None
        return room_free_periods

    def generate_reservations(
            self, rooms, start_date, end_date, start_time, end_time, interval,
            subject, description, attendees
    ):
        result = []
        room_free_periods = self.get_room_free_periods(
            rooms=rooms,
            start_date=datetime.combine(start_date, start_time),
            end_date=datetime.combine(end_date, end_time),
            interval=interval
        )

        series = ReservationsSeries.objects.create()
        for day in self.get_days_iter(start_date, end_date):
            start = datetime.combine(day, start_time)
            end = datetime.combine(day, end_time)
            result.append(
                self.generate_reservation(
                    room_free_periods=room_free_periods,
                    series=series,
                    rooms=rooms,
                    start_datetime=start,
                    end_datetime=end,
                    interval=interval,
                    subject=subject,
                    description=description,
                    attendees=attendees
                )
            )
        return result

    @classmethod
    def generate_reservation(
            cls, room_free_periods, series, rooms, start_datetime, end_datetime,
            interval, subject, description, attendees
    ):
        room, start, end, state = cls.resolve_reservation_params(
            room_free_periods, rooms, start_datetime, end_datetime, interval
        )
        reservation = Reservation.objects.create(
            subject=subject,
            description=description,
            room=room,
            series=series,
            start_time=start,
            end_time=end,
            state=state
        )
        reservation.attendees.add(*attendees)
        return reservation

    @classmethod
    def resolve_reservation_params(
            cls, room_free_periods, rooms, start_datetime, end_datetime,
            interval
    ):
        chosen_room = None
        start_date = None
        for room in rooms:
            if room.id in room_free_periods:
                free_periods = room_free_periods[room.id]
                free_periods = sorted(
                    free_periods,
                    key=lambda obj: obj['interval']
                )
                for period in free_periods:
                    period_start = ensure_timezone(period['start_date'])
                    period_end = ensure_timezone(
                        period['start_date']
                    ) + timedelta(
                        minutes=period['interval']
                    )
                    if period_start >= ensure_timezone(start_datetime) and \
                            period_end <= ensure_timezone(end_datetime):
                        chosen_room = room
                        start_date = period['start_date']
                        break
                if chosen_room:
                    break
        state = (
            ReservationStates.RESERVED
            if chosen_room
            else ReservationStates.COLLISION
        ).value
        chosen_room = chosen_room or rooms[0]
        start_date = start_date or start_datetime
        end_date = start_date + timedelta(minutes=interval)
        return chosen_room, start_date, end_date, state


class Reservation(models.Model):
    subject = models.CharField(max_length=255, default='')
    description = models.TextField(default='')
    attendees = models.ManyToManyField(Profile)
    room = models.ForeignKey(Room, related_name='reservations')
    series = models.ForeignKey(ReservationsSeries)

    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    state = models.IntegerField(choices=ReservationStates.get_choices())

    objects = ReservationManager()

    def __unicode__(self):
        return '%s | %s' % (self.subject, self.description)
