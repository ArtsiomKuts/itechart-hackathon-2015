$(document).ready(function () {

    $.when(
        apiWrapper.returnReservations(),
        apiWrapper.getAllRoomAttributes(),
        apiWrapper.getAllRooms(),
        apiWrapper.getAllUsers()
    ).done(
        function (reservations, attributes, rooms, users) {
            var data = {
                reservations: reservations,
                attributes: attributes,
                rooms: rooms,
                users: users
            };

            var serialized_rooms = [];
            var serialized_users = [];
            var serialized_reservations = [];
            var serialized_attributes = [];

            function color_by_state(state) {
                var color = ['#A9EF67', '#D3D3D3', '#EF6262'];
                return color[state];
            };

            var map = [];
            $.each(rooms[0], function (index, value) {
                serialized_rooms.push(
                    {id: value.id, title: value.name}
                );
                map[value.id] = value.name;
            });

            $.each(reservations[0], function (index, value) {

                serialized_reservations.push(
                    {
                        id: value.id,
                        roomId: value.room.id,
                        title: value.subject +" | "+ map[value.room.id],
                        start: value.start_date,
                        end: value.end_date,
                        color: color_by_state(value.state)
                    }
                );
            });

                   $('#calendar').fullCalendar({
            eventResourceField: 'roomId',
            resources: serialized_rooms,
            events: serialized_reservations,
            weekends: false,
            header: {
                left: 'prev, next today',
                center: 'title',
                right: 'month, agendaWeek, agendaDay'
            },
            firstDay: 1,
            allDayText: 'all day event',
            minTime: "08:30:00",
            maxTime: "22:00:00",
            height: '600',
            editable: true,
            slotEventOverlap: false})
        });

});
