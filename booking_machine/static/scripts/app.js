window.reservationState = {
    attendees: [],
    room_attrs: []
};

var init = function($, _, utils, attributes, people, rooms) {

    var compilers = {};
    var templates = [['personCompiler', '#personInputTemplate'], ['attributesCompiler', '#attributesTemplate']
        , ['popupsCompiler', '#popupInfoTemplate']];
    var i = 0, len = templates.length;
    for(; i < len; i++) {
        compilers[templates[i][0]] = utils.renderTemplate.bind(null, $(templates[i][1]));
    }

    var peopleName = _.map(people, function(elem) {
       return elem['email'];
    });

    var overlay = $('overlay');

    var addPeopleButton = $('#add-people-button');
    var peopleContainer = $('#peopleContainer');

    peopleContainer.on('click', '.remove-person', function() {
        $(this).parents('.person-container').remove();
        peopleContainer.find('.person-container').each(function(ind, e) {
            $(e).find('input').attr('name', 'person_' + ind);
        });
    });

    $('.popup-open-button').magnificPopup({
        type: 'inline',
        removalDelay: 500,
        callbacks: {
            beforeOpen: function() {
                console.log(this, this.st);
                this.st.mainClass = (this.st.el.data('effect') || 'mfp-3d-unfold');
            },
            beforeClose: function() {

            }
        },
        midClick: true,
        closeOnBgClick: false
    });

    utils.initTypeahead(peopleName, "person_0", peopleContainer.find('input'));

    addPeopleButton.on('click', function(e) {
        e.preventDefault();
        var count = peopleContainer.find('.person-container').length;
        var person = {name: 'person_' + count};
        var $person = $(compilers['personCompiler'](person));
        //console.log($person, peopleContainer);
        peopleContainer.append($person);
        utils.initTypeahead(peopleName, person['name'], $person.find('input'));
    });



    var getIdFromName = function(names) {
        var successful = [];
        var failed = [];
        _.each(names, function(name) {
            var man = _.findWhere(people, {'email': name});
            if(man) {
                successful.push(man['id']);
            } else {
                failed.push(name);
            }
        });
        return {success: successful, fail: failed};
    };

    $('#attendeesForm').on('submit', function(e) {
        e.preventDefault();
        var values = _.map($(this).find('input.native-input').not('.tt-hint'), function(el) {
            var $el = $(el);
            return {'name': $el.val(), '$name': $el.attr('name'), '$el': $el};
        });
        var namesOnly = _.map(values, function(el) {
            return el['name'];
        });
        var results = getIdFromName(namesOnly);
        if(results['fail'].length) {
            _.each(results['fail'], function(element) {
                var elem = _.findWhere(values, {'name': element});
                if(elem) {
                    elem['$el'].parents('.person-container').addClass('error');
                }
            });
        } else {
            window.reservationState['attendees'] = results['success'];
            $('#chooseOptions').trigger('click');
        }
    });

    var initIntervalForStart = function() {
        $('[name="meeting-interval-start"]').timepicker({
            'minTime': '8:30am',
            'maxTime': '22:00pm',
                'timeFormat': 'H:i:s',
            'showDuration': true
        });
        $('[name="meeting-interval-start"]').on('changeTime', function() {
            $('[name="meeting-interval-end"]').timepicker({
                'minTime': $(this).val(),
                'maxTime': '22:00pm',
                'timeFormat': 'H:i:s',
                'showDuration': true
            })
        });
    };


    var initIntervalForEnd = function() {
        $('[name="meeting-interval-end"]').timepicker({
            'minTime': '8:30am',
            'maxTime': '21:30pm',
                'timeFormat': 'H:i:s',
            'showDuration': true
        });
        $('[name="meeting-interval-end"]').on('changeTime', function () {
            $('[name="meeting-interval-start"]').timepicker({
                'minTime': '8:30am',
                'timeFormat': 'H:i:s',
                'maxTime': $(this).val(),
                'showDuration': true
            })
        });
    };

    $('[name="meeting-date-start"]').datepicker({
        'format': 'yyyy-m-d',
        'autoclose': true
    });

    $('[name="meeting-date-end"]').datepicker({
        'format': 'yyyy-m-d',
        'autoclose': true
    });

    _.each(attributes, function(el) {
        el['nm'] = el['id'] + '_' + el['name'];
        var $attr = $(compilers['attributesCompiler'](el));
        $("#chooseOptionsPopup .person-container").append($attr);
        $attr.find('.value-input').on('change', function() {
            if($(this).is(":checked")) {
                $attr.find('input').not(".value-input").removeAttr("disabled");
            } else {
                $attr.find('input').not(".value-input").attr("disabled", 'true').removeAttr('checked');
            }
        })
    });

    $('#delete-failed').on('click', function() {
        if(window.failedReservation) {
            apiWrapper.deleteReservations(window.failedReservation).then(function() {
                $.magnificPopup.close();
            });
        }
    });

    $('#meetingForm').on('submit', function(e) {
        e.preventDefault();
        var $this = $(this);
        $this.find('input, textarea').each(function(ind, el) {
            if(!$(el).val()) {
                $(el).addClass('error');
            }
        });
        if($this.find(".error").length) {
            return;
        } else {
            _.each($(this).find('.value-input'), function(el) {
               var $el = $(el);
               if(($el).is(':checked')) {
                   if($el.siblings('.value-input').is(':checked')) {
                       window.reservationState['room_attrs'].push({'id': $el.attr('name'), 'mandatory': true});
                   } else {
                       window.reservationState['room_attrs'].push({'id': $el.attr('name'), 'mandatory': false});
                   }
                }
            });
            window.reservationState.interval = $this.find('[name="interval"]').val();
            window.reservationState.start_time = $this.find('[name="meeting-interval-start"]').val();
            window.reservationState.end_time = $this.find('[name="meeting-interval-end"]').val();
            window.reservationState.subject = $this.find('[name="subject"]').val();
            window.reservationState.description = $this.find('[name="description"]').val();
            window.reservationState.start_date = $this.find('[name="meeting-date-start"]').val().replace(/\//g, '-');
            window.reservationState.end_date = $this.find('[name="meeting-date-end"]').val().replace(/\//g, '-');
            apiWrapper.addReservations(window.reservationState).then(function(data) {
                console.log(data);
                var bad = 0;
                _.map(data, function(el) {
                    if(el.state == 2) {
                        bad++;
                    }
                });
                if(bad) {
                    window.failedReservation = data[0].series_id;
                    $('#failPopup').find('.form-title').text(bad + ' out of ' + data.length + ' failed. Should we delete the whole session?')
                    $('#failPopupLink').trigger('click');
                } else {
                    apiWrapper.returnReservations().then(function(data) {
                        var serialized_reservations = [];
                        function color_by_state(state) {
                            var color = ['#A9EF67', '#D3D3D3', '#EF6262'];
                            return color[state];
                        }
                        $.each(data, function (index, value) {
                            serialized_reservations.push(
                                {
                                    id: value.id,
                                    roomId: value.room.id,
                                    title: value.subject,
                                    start: value.start_date,
                                    end: value.end_date,
                                    color: color_by_state(value.state)
                                }
                            );
                        });
                        $('#calendar').fullCalendar({
                            events: serialized_reservations
                        });
                        $('#calendar').fullCalendar('rerenderEvents');
                        $.magnificPopup.close();
                    })
                }
            }, function(data) { console.log('error', data); });
        }
    });

    initIntervalForStart();
    initIntervalForEnd();



}.bind(null, jQuery, _, window.utils);

//window.apiWrapper.peopleMock().then(function(data) {
//    init(null, data, null);
//});

$.when(window.apiWrapper.getAllUsers(), window.apiWrapper.getAllRoomAttributes()).done(function(people, attrs) {
    init(attrs[0], people[0], null);
//=======
//window.apiWrapper.getAllUsers().then(function(data) {
//    init(null, data, null);
//>>>>>>> origin/master
});