window.apiWrapper = window.apiWrapper || (function($) {
    var csrftoken = $.cookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    var peopleMock = function() {
        var def = $.Deferred();
        setTimeout(function(){ def.resolve(
           [
                {
                    'id': 1,
                    'email': 'blah-blah'
                },
                {
                    'id': 2,
                     'email': 'fock you'
                },
                {
                    'id': 3,
                    'email': 'floff'
                },
                {
                    'id': 4,
                    'email': 'fjoff'
                },
                {
                    'id': 5,
                    'email': 'bob'
                }
           ]
        ); }, 1000);
        return def.promise();
    };

    var ajax = $.ajax;
    var returnReservations = function() {
        return ajax({
            type: 'GET',
            url: 'api/reservations/'
        });
    };

    var addReservations = function(data) {
        return ajax({
            type: 'POST',
            url: 'api/reservations/',
            data: JSON.stringify(data),
            contentType: 'application/json'
        });
    };

    var getAllRooms = function() {
        return ajax({
            type: 'GET',
            url: 'api/rooms'
        });
    };

    var getAllUsers = function() {
        return ajax({
            type: 'GET',
            url: 'api/users'
        });
    };

    var getAllProfiles = function() {
        return ajax({
            type: 'GET',
            url: 'api/profiles'
        });
    };

    var getAllRoomAttributes = function() {
        return ajax({
            type: 'GET',
            url: 'api/attrs'
        });
    };

    var deleteReservations = function(param) {
        return ajax({
            type: 'DELETE',
            url: 'api/series/' + param
        });
    };

    return {
        addReservations: addReservations,
        returnReservations: returnReservations,
        getAllUsers: getAllUsers,
        getAllRooms: getAllRooms,
        getAllRoomAttributes: getAllRoomAttributes,
        peopleMock: peopleMock,
        getAllProfiles: getAllProfiles,
        deleteReservations: deleteReservations
    }
})(jQuery);
